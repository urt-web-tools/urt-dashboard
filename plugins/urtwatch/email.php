<html>
<head>
<META HTTP-EQUIV="REFRESH" CONTENT="60">
</head>
<body>
<?php
$mail_to = "msweer1, rik.declercq@uzleuven.be, paul, philip.miseur@uzleuven.be, laszlo.penninckx@uzleuven.be";

function fetchPlayers()
{
$host = "127.0.0.1";
$port = 27960;		
$timeout = 15;                               // Default timeout for the php socket (seconds)
$length = 2048;                               // Packet length (should this be larger?)
$protocol = 'udp';                            // Default protocol for sending query
$magic = "\377\377\377\377";                  // Magic string to send via UDP
$pattern = "/$magic" . "print\n/";
$pattern2 = "/$magic" . "statusResponse\n/";

$players = array(); // List of players

if(!function_exists("socket_create")) die("<font color=red>socket support missing!</font>");

// Create the UDP socket
$socket = socket_create (AF_INET, SOCK_DGRAM, getprotobyname ($protocol));
if ($socket)
{
	if (socket_set_nonblock ($socket))
	{
		$time = time();
		$error = "";
		while (!@socket_connect ($socket, $host, $port ))
		{	
			$err = socket_last_error ($socket);
			if ($err == 115 || $err == 114)
			{
				if ((time () - $time) >= $timeout)
				{
					socket_close ($socket);
					echo "Error! Connection timed out.";
				}
				sleep(1);
				continue;
			}
		}

		// Verify if an error occured
		if( strlen($error) == 0 )
		{
			socket_write ($socket, $magic . "getstatus\n");
			$read = array ($socket);
			$out = "";
			$empty = array();
			while (socket_select ($read, $empty, $empty, 1))
			{
				$out .= socket_read ($socket, $length, PHP_BINARY_READ);
			}

			if ($out == "")
				echo "<center><font color=red><h2>Unable to connect to server...</h2></font></center>\n";
			
			socket_close ($socket);
			$out = preg_replace ($pattern, "", $out);
			$out = preg_replace ($pattern2, "", $out);
				$all = explode( "\n", $out );
			$params = explode( "\\", $all[0] );
			array_shift( $params );
			$temp = count($params);
			for( $i = 0; $i < $temp; $i++ ) {
				$params[ strtolower($params[$i]) ] = $params[++$i];
			}
				
			for( $i = 1; $i < count($all) - 1; $i++ ) {
				$pos = strpos( $all[$i], " " );
				$score = substr( $all[$i], 0, $pos );
				$pos2 = strpos( $all[$i], " ", $pos + 1 );
				$ping = substr( $all[$i], $pos + 1, $pos2 - $pos - 1 );
				$name = substr( $all[$i], $pos2 + 2 );
				$name = substr( $name, 0, strlen( $name ) - 1);

				$player = array( $name, $score, $ping );
				$players[] = $name;
			}
		} else {
			echo "Unable to connect to server.";
		}
	} else {
		echo "Error! Unable to set nonblock on socket.";
	}
} else {
	echo "The server is DOWN!";
}
return $players;
}

session_start();
if (!isset($_SESSION['players'])) $_SESSION['players'] = array();
$old_players = $_SESSION['players'];
$new_players = fetchPlayers();
echo "player count = ". count($new_players)."<br>\n";
foreach ($new_players as $player) {
  if (!in_array($player, $old_players)) {
    echo "new player $player <br>\n";
	mail($mail_to, "player $player has joined urt server", "tist moment om ook te joinen", 'From: Urt Uberwatch <uberwatch@wewantyoutojoinurt.com>');
  }
}
$_SESSION['players'] = $new_players; 
$now = new DateTime();
echo "<br>last update: ".$now->format('Y-m-d H:i:s');
?>
</body>
</html>