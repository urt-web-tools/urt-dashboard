<?php
require_once('../../../util/rcon.php');

function fetchPlayers() {
	global $rcon_host;

	$players = array(); // List of players
	$all = send_rcon_cmd($rcon_host, NULL, "getstatus");
	//print_r($all);
	$params = explode( "\\", $all[0] );
	array_shift( $params );
	$temp = count($params);
	for( $i = 0; $i < $temp; $i++ ) {
		$params[ strtolower($params[$i]) ] = $params[++$i];
	}
	for( $i = 1; $i < count($all) - 1; $i++ ) {
		$pos = strpos( $all[$i], " " );
		$score = substr( $all[$i], 0, $pos );
		$pos2 = strpos( $all[$i], " ", $pos + 1 );
		$ping = substr( $all[$i], $pos + 1, $pos2 - $pos - 1 );
		$name = substr( $all[$i], $pos2 + 2 );
		$name = substr( $name, 0, strlen( $name ) - 1);

		$player = array( $name, $score, $ping );
		$players[] = $name;
	}
	return $players;
}
function admin_say($txt) {
	global $rcon_host;
	global $rcon_pwd;
	$cmd = 'bigtext "'.$txt.'"';
	echo "fun: $txt<br>\n";
	send_rcon_cmd($rcon_host, $rcon_pwd, $cmd);
}

function say_randomline($lines) {
	admin_say($lines[rand(0,count($lines)-1)]);
}

function funtalk($players) {
	global $plugin_fun_lines;
	global $plugin_fun_chance;
	if (array_key_exists(count($players),$plugin_fun_lines)) {
		if (rand(1,$plugin_fun_chance) == 1) say_randomline($plugin_fun_lines[count($players)]);
	}
}

session_start();
if (!isset($_SESSION['players'])) $_SESSION['players'] = array();
$old_players = $_SESSION['players'];
$new_players = fetchPlayers();
echo "player count = ". count($new_players)."<br>\n";
foreach ($new_players as $player) {
  if (!in_array($player, $old_players)) {
    echo "new player $player <br>\n";
	mail($plugin_mail_to, "player $player has joined urt server", $plugin_msg, $plugin_from);
  }
}
$_SESSION['players'] = $new_players; 
if ($plugin_fun_lines_enabled == true) {
	funtalk($new_players);
}
?>