<?php
	require_once('../../../util/rcon.php');

	$filename = $urt_home.'q3ut4/mapcycle.txt';
	echo "mapcycle file: ". $filename."<br>\n";
	$old_maps = explode(PHP_EOL, file_get_contents($filename));

	$default_maps = explode(PHP_EOL, file_get_contents('defaultmaps.txt'));
	//print_r($default_maps);
	function nozpak($map) {
		return !strpos($map, 'zpak');
	}
	$maps = glob($urt_home.'q3ut4/*.pk3');
	$maps = array_filter($maps, 'nozpak');
	array_multisort(    array_map( 'filectime', $maps ),    SORT_NUMERIC,    SORT_DESC,    $maps);
	function mapname($filename) {
		return substr(substr($filename, strrpos($filename, '/')+1), 0, -4);
	}
	$maps = array_map('mapname', $maps);
	$maps = array_merge($maps, $default_maps);
	foreach($maps as $map) {
		if (in_array($map, $default_maps)) {
			print "<i>$map</i>";
		} else {
			print "$map";
		}
		if (!in_array($map, $old_maps)) {
			print " <b>NEW!</b>";
			$new_map = $map;
		}
		print "<br>\n";
	}
	echo "<br>writing mapcycle $filename<br>\n";
   file_put_contents($filename, implode(PHP_EOL, $maps));
   if (isset($new_map)) {
   echo "launching new map $new_map<br>\n";
		send_rcon_cmd($rcon_host, $rcon_pwd, 'bigtext woohaa, new map detected!!');
		sleep(5);
		send_rcon_cmd($rcon_host, $rcon_pwd, 'reload');		
		sleep(5);
		send_rcon_cmd($rcon_host, $rcon_pwd, 'map '.$new_map);		
   }

?>