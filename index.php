<?php

require_once('../util/rcon.php');
require_once('config.php');

function send($cmd) {
	global $rcon_host;
	global $rcon_pwd;
	echo "Executing cmd:<br>$cmd<br>";
	send_rcon_cmd($rcon_host, $rcon_pwd, $cmd);
	echo "OK<br>\n";

}

if (isset($_POST['action']) && $_POST['action']=='send') {
	if ($_POST['cmd'] == 'yell') {
		send('bigtext "'.$_POST['param1'].'"');
	}
	if ($_POST['cmd'] == 'raw') {
		send($_POST['param1']);
	}
	if ($_POST['cmd'] == 'startmap') {
		send("map ".$_POST['param1']);
	}
}
function add_1clickbutton($name, $cmd) {
	echo '<form name="input" action="index.php" method="post">
			<input type="hidden" name="action" value="send" />
			<input type="hidden" name="cmd" value="raw" />
			<input type="hidden" name="param1" value="'.$cmd.'" />
			<input type="submit" value="'.$name.'" />
			</form>';
}
function add_plugin($plugin) {
	echo $plugin
	.'<INPUT TYPE="button" VALUE="Start cron" onclick="window.open(\'plugins/'.$plugin.'/\')">'
	.'<INPUT TYPE="button" VALUE="Run once" onclick="window.open(\'plugins/'.$plugin.'/?runonce\')">'
	."<br>\n";
}
function add_plugins() {
	$plugins = glob('plugins/*', GLOB_ONLYDIR);
	function pluginname($path) {
		return substr($path, strpos($path, '/')+1);
	}
	$plugins = array_map('pluginname', $plugins);
	foreach ($plugins as $plugin) {
		add_plugin($plugin);
	}
}

function list_map_options($sort = FALSE) {
	global $urt_home;
	$filename = $urt_home.'q3ut4/mapcycle.txt';
	$maps = explode(PHP_EOL, file_get_contents($filename));
	if ($sort === TRUE) {
		sort($maps);
	}
	foreach ($maps as $map) {
		echo '<option value="'.$map.'">'.$map."</option>\n";
	}
}
?>
<script type="text/javascript">
document.title = 'URT dashboard';
</script>
<h3>1-click actions</h3>
<?php add_1clickbutton('Reload', 'reload'); ?>
<?php add_1clickbutton('Cycle map', 'cyclemap'); ?>

<h3>plugins</h3>
<?php add_plugins(); ?>

<h3>admin yell</h3>
<form name="input" action="index.php" method="post">
<input type="hidden" name="action" value="send" />
<input type="hidden" name="cmd" value="yell" />
text: <input type="text" name="param1" />
<input type="submit" value="Submit" />
</form>

<h3>raw console cmd</h3>
<form name="input" action="index.php" method="post">
<input type="hidden" name="action" value="send" />
<input type="hidden" name="cmd" value="raw" />
cmd: <input type="text" name="param1" />
<input type="submit" value="Submit" />
</form>

<h3>start map (mapcycle order)</h3>
<form action="index.php" method="post">
<input type="hidden" name="action" value="send" />
<input type="hidden" name="cmd" value="startmap" />
select map: 
<select name="param1">
<?php list_map_options(); ?>
</select>
<input type="submit" value="Submit" />

</form><h3>start map (alphabetic order)</h3>
<form action="index.php" method="post">
<input type="hidden" name="action" value="send" />
<input type="hidden" name="cmd" value="startmap" />
select map: 
<select name="param1">
<?php list_map_options(TRUE); ?>
</select>
<input type="submit" value="Submit" />
</form>